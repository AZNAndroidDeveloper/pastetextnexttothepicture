package uz.azn.lesson26homework

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.TextUtils
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import uz.azn.lesson26homework.databinding.FragmentBookBinding


class BookFragment : Fragment(R.layout.fragment_book) {
    lateinit var binding :FragmentBookBinding
    @SuppressLint("UseCompatLoadingForDrawables")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentBookBinding.bind(view)
        val text = getString(R.string.text)
        val imageOne  = binding.icon.resources.getDrawable(R.drawable.zumrad_va_qimmat)
        val leftMargin = imageOne.intrinsicWidth-480
        binding.icon.setBackgroundDrawable(imageOne)
        val spannable = SpannableString(text)
        spannable.setSpan(MyLeadingMarginSpan2(8,leftMargin),0 ,spannable.length,0)
        binding.messageView.text = spannable


  }




}